﻿using System;

namespace week3pt2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var myName = "Michaela";
            Console.WriteLine(myName);
            Console.WriteLine("In which month were you born?");
            var month = (Console.ReadLine());

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
